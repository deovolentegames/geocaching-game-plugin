﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MapzenGo.Helpers.VectorD;
using UnityEngine;


namespace MapzenGo.Helpers
{
	/// <summary>
	/// Mapzen based geolocation helper functions.
	/// </summary>
	/// <remarks>
	/// Written by Baran Kahyaoglu for MapzenGo - <see href="http://barankahyaoglu.com"/>, <see href="https://github.com/brnkhy/MapzenGo"/>. 
	/// <para>
	/// Edited by Rory Juranovich for Geolocation Unity Plugin - <see href="http://deovolente.games/"/>. 
	/// </para> 
	/// <seealso href="http://stackoverflow.com/questions/12896139/geographic-coordinates-converter"/>
	/// </remarks>
	public static class GM
    {
        public const int TileSize = 256;
		public const int EarthRadius = 6378137;
		public const double InitialResolution = 2 * Math.PI * EarthRadius / TileSize;
		public const double OriginShift = 2 * Math.PI * EarthRadius / 2;

        public static Vector2d LatLonToMeters(Vector2d v)
        {
            return LatLonToMeters(v.x, v.y);
        }

		/// <summary>
		/// Converts given lat/lon in WGS84 Datum to Spherical Mercator EPSG:900913.
		/// </summary>
		/// <param name="lat">Latitude in WGS84 Datum.</param>
		/// <param name="lon">Longitude in WGS84 Datum.</param>
		/// <returns>Vector2d of Spherical Mercator EPSG:900913 coordinates.</returns>
		public static Vector2d LatLonToMeters(double lat, double lon)
        {
            var p = new Vector2d();
            p.x = (lon * OriginShift / 180);
            p.y = (Math.Log(Math.Tan((90 + lat) * Math.PI / 360)) / (Math.PI / 180));
            p.y = (p.y * OriginShift / 180);
            return new Vector2d(p.x, p.y);
        }

		/// <summary>
		/// Converts pixel coordinates in given zoom level of pyramid pixel to Spherical Mercator EPSG:900913.
		/// </summary>
		/// <param name="p">Vector2d of pixel coordinates in pyramid pixel.</param>
		/// <param name="zoom">TMS zoom level.</param>
		/// <returns>Vector2d of Spherical Mercator EPSG:900913 coordinates.</returns>
		public static Vector2d PixelsToMeters(Vector2d p, int zoom)
        {
            var res = Resolution(zoom);
            var met = new Vector2d();
            met.x = (p.x * res - OriginShift);
            met.y = -(p.y * res - OriginShift);
            return met;
        }

		/// <summary>
		/// Converts Spherical Mercator EPSG:900913 to pyramid pixel coordinates in given zoom level.
		/// </summary>
		/// <param name="m">Vector2d of Spherical Mercator EPSG:900913 coordinates.</param>
		/// <param name="zoom">TMS zoom level.</param>
		/// <returns>Vector2d of pyramid pixel coordinates.</returns>
		public static Vector2d MetersToPixels(Vector2d m, int zoom)
        {
            var res = Resolution(zoom);
            var pix = new Vector2d();
            pix.x = ((m.x + OriginShift) / res);
            pix.y = ((-m.y + OriginShift) / res);
            return pix;
        }

		/// <summary>
		/// Returns a TMS (NOT Google!) tile containing given pyramid pixel coordinates.
		/// </summary>
		/// <param name="p">Vector2d of pyramid pixel coordinates.</param>
		/// <returns>Vector2d of TMS Tile X,Y coordinates.</returns>
		public static Vector2d PixelsToTile(Vector2d p)
        {
            var t = new Vector2d();
            t.x = (int)Math.Ceiling(p.x / (double)TileSize) - 1;
            t.y = (int)Math.Ceiling(p.y / (double)TileSize) - 1;
            return t;
        }

        public static Vector2d PixelsToRaster(Vector2d p, int zoom)
        {
            var mapSize = TileSize << zoom;
            return new Vector2d(p.x, mapSize - p.y);
        }

		/// <summary>
		/// Returns TMS tile containing given Spherical Mercator EPSG:900913 coordinates.
		/// </summary>
		/// <remarks>
		/// <seealso cref="MetersToPixels"/>
		/// <seealso cref="PixelsToTile"/>
		/// </remarks>
		/// <param name="m">Vector2d of Spherical Mercator EPSG:900913 coordinates.</param>
		/// <param name="zoom">TMS zoom level.</param>
		/// <returns>Vector2d of TMS Tile X,Y coordinates.</returns>
		public static Vector2d MetersToTile(Vector2d m, int zoom)
        {
            var p = MetersToPixels(m, zoom);
            return PixelsToTile(p);
        }

		/// <summary>
		/// Returns bounds of the given TMS tile in Spherical Mercator EPSG:900913. 
		/// </summary>
		/// <remarks>
		/// Bounds start with "origin" (minimum) of tile in south-west.
		/// </remarks>
		/// <param name="t">Tile in TMS coordinates.</param>
		/// <param name="zoom">TMS zoom level.</param>
		/// <returns>Vector2d of Spherical Mercator EPSG:900913 bounds.</returns>
		public static RectD TileBounds(Vector2d t, int zoom)
        {
            var min = PixelsToMeters(new Vector2d(t.x * TileSize, t.y * TileSize), zoom);
            var max = PixelsToMeters(new Vector2d((t.x + 1) * TileSize, (t.y + 1) * TileSize), zoom);
            return new RectD(min, max - min);
        }

        public static Vector2d MetersToLatLon(Vector2d m)
        {
            var ll = new Vector2d();
            ll.x = (m.x / OriginShift) * 180;
            ll.y = (m.y / OriginShift) * 180;
            ll.y = 180 / Math.PI * (2 * Math.Atan(Math.Exp(ll.y * Math.PI / 180)) - Math.PI / 2);
            return ll;
        }

        //Returns bounds of the given tile in latutude/longitude using WGS84 datum
        //public static RectD TileLatLonBounds(Vector2d t, int zoom)
        //{
        //    var bound = TileBounds(t, zoom);
        //    var min = MetersToLatLon(new Vector2d(bound.Min.x, bound.Min.y));
        //    var max = MetersToLatLon(new Vector2d(bound.Min.x + bound.Size.x, bound.Min.y + bound.Size.y));
        //    return new RectD(min.x, min.y, Math.Abs(max.x - min.x), Math.Abs(max.y - min.y));
        //}

        //Resolution (meters/pixel) for given zoom level (measured at Equator)
        public static double Resolution(int zoom)
        {
            return InitialResolution / (Math.Pow(2, zoom));
        }

        public static double ZoomForPixelSize(double pixelSize)
        {
            for (var i = 0; i < 30; i++)
                if (pixelSize > Resolution(i))
                    return i != 0 ? i - 1 : 0;
            throw new InvalidOperationException();
        }

        // Switch to Google Tile representation from TMS
        public static Vector2d ToGoogleTile(Vector2d t, int zoom)
        {
            return new Vector2d(t.x, ((int)Math.Pow(2, zoom) - 1) - t.y);
        }

        // Switch to TMS Tile representation from Google
        public static Vector2d ToTmsTile(Vector2d t, int zoom)
        {
            return new Vector2d(t.x, ((int)Math.Pow(2, zoom) - 1) - t.y);
        }
    }
}
