﻿using System;
using System.Collections.Generic;
using System.Linq;
using MapzenGo.Helpers;
using UnityEngine;
using MapzenGo.Helpers.VectorD;

namespace MapzenGo.Models
{
    public class DynamicTileManager : TileManager
    {
        [SerializeField] private Rect _centerCollider;
        [SerializeField] private int _removeAfter;
        [SerializeField] private bool _keepCentralized;

		private Vector2d offset;
		private RectD centerRect;
		private Vector2d centerTileCornerPos;
		private float scaleMultiplier;

		public override void Start()
        {
            base.Start();
            _removeAfter = Math.Max(_removeAfter, Range * 2 + 1);
            var rect = new Vector2(TileSize, TileSize);
            _centerCollider = new Rect(Vector2.zero - rect / 2 , rect);
			offset = Vector2d.zero;

			// Set initial position
			Centralize(GM.LatLonToMeters(Latitude, Longitude));
			scaleMultiplier = (float)(TileSize / centerRect.Width);
		}

        public override void Update()
        {
            base.Update();
            UpdateTiles();
        }
		
        private void UpdateTiles()
        {
			var meters = GM.LatLonToMeters(Latitude, Longitude);
			offset = meters - centerTileCornerPos;

			_centerCollider.position = (-offset).ToVector2() * scaleMultiplier;

			if (!_centerCollider.Contains(Vector2.zero, true))
			{
				Centralize(meters);
			}
			foreach (var tile in Tiles.Values)
			{
				tile.transform.position = ((tile.Rect.Min - centerTileCornerPos - offset + centerRect.Size / 2).ToVector3()) * scaleMultiplier;
			}

			/*
			// ========================= DEBUGGING =========================
			var tileTest = GM.MetersToTile(meters, Zoom);
			DebugText.Log("tms " + CenterTms + " | offset " + offset +
						"\ntile " + tileTest + " | meters " + meters +
						"\n " + centerTileCornerPos);
			// Debug.Log("tms " + CenterTms + " | offset " + offset);
			// ======================= END DEBUGGING =======================

			if (Math.Abs(_centerCollider.center.x) > 10f || Math.Abs(_centerCollider.center.y) > 10f)
			{
				Debug.Break();
				throw new System.Exception("Offset is too large, the map should stay closer to the center: " + Math.Abs(_centerCollider.center.x) + ", " + Math.Abs(_centerCollider.center.y));
			}
			*/
		}

		private void Centralize(Vector2d meters)
		{
			centerRect = GM.TileBounds(CenterTms, Zoom);
			centerTileCornerPos = new Vector2d(centerRect.Min.x, centerRect.Min.y + centerRect.Size.y);

			//player movement in TMS tiles
			var tileDif = GetMovementVector();

			//move locals
			CenterTms = GM.MetersToTile(meters, Zoom);
			CenterInMercator = GM.TileBounds(CenterTms, Zoom).Center;
			// Debug.Log(CenterTms + " ||| " + _centerCollider.center);

			//create new tiles
			LoadTiles(CenterTms, CenterInMercator);
			UnloadTiles(CenterTms);
		}

        private void UnloadTiles(Vector2d currentTms)
        {
            var rem = new List<Vector2d>();
            foreach (var key in Tiles.Keys.Where(x => x.ManhattanTo(currentTms) > _removeAfter))
            {
                rem.Add(key);
                Destroy(Tiles[key].gameObject);
            }
            foreach (var v in rem)
            {
                Tiles.Remove(v);
            }
		}

		private Vector2 GetMovementVector()
		{
			var tileDif = Vector2.zero;
			if (0 < Math.Min(_centerCollider.xMin, _centerCollider.xMax))
				tileDif.x = -1;
			else if (0 > Math.Max(_centerCollider.xMin, _centerCollider.xMax))
				tileDif.x = 1;

			if (0 < Math.Min(_centerCollider.yMin, _centerCollider.yMax))
				tileDif.y = 1;
			else if (0 > Math.Max(_centerCollider.yMin, _centerCollider.yMax))
				tileDif.y = -1; //invert axis  TMS vs unity
			return tileDif;
		}
		
		void OnDrawGizmos()
		{
            Gizmos.color = Color.magenta;
			Gizmos.DrawWireCube(_centerCollider.center.ToVector3xz() + Vector3.up * 0.01f, _centerCollider.size.ToVector3xz());
		}
    }
}
