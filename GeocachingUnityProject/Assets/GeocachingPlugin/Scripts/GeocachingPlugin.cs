﻿using System;
using UnityEngine;

namespace GeocachingAndroidPlugin
{
	/// <summary>
	/// The component that creates and manages the native android plugin.
	/// </summary>
	/// <remarks>
	/// To see and example of how to use this, see <see cref="PlayerLocation"/>.
	/// <para>
	/// TODO: change into a singleton, or make this more suited to having multiple of these existing at once. 
	/// </para>
	/// </remarks>
	public class GeocachingPlugin : MonoBehaviour
	{
		// TODO: remove latitude, longitude and totalDistanceTravelled.
		public double latitude { get; private set; }
		public double longitude { get; private set; }
		public float totalDistanceTravelled { get; private set; }

		[Space(5)]
		[Header("Location Configuration")]
		public float locationAccuracy = 10;
		/// <summary>
		/// Minimum speed to trigger location updates in 
		/// </summary>
		public float minSpeed = 0.1f;
		public float maxSpeed = 5f;
		public int locationInterval = 7000;
		public int locationFastestInterval = 1000;

		// TODO: Decide whether I should use C# Actions or UnityActions 
		// http://jacksondunstan.com/articles/3335
		// private Dictionary<string, UnityAction> uListeners;
		private event Action<double, double, int> aListeners;

		AndroidJavaObject pluginClass;

		/// <summary>
		/// Initialises this object and creates an instance of the native plugin.
		/// </summary>
		void Start()
		{
			if (Application.platform == RuntimePlatform.Android)
			{
				using (var javaUnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
				{
					using (var currentActivity = javaUnityPlayer.GetStatic<AndroidJavaObject>("currentActivity"))
					{
						pluginClass = new AndroidJavaObject("com.deovolentegames.geocachingunityplugin.GeocachingGamePlugin");
						pluginClass.Call("StartLocationCacheService", currentActivity, new LocationPluginListener(this));
						pluginClass.Call("SetConfiguration", locationAccuracy, minSpeed, maxSpeed, locationInterval, locationFastestInterval);
					}
				}
			}
			// StartCoroutine(LocationTest());
		}

		void OnDestroy()
		{
			// TODO: Tell the plugin to get rid of everything
		}

		/// <summary>
		/// Pauses and unpauses the native plugin instance. 
		/// </summary>
		/// <param name="pauseStatus">True if pausing, false if unpausing.</param>
		void OnApplicationPause(bool pauseStatus)
		{
			if (pluginClass != null && Application.platform == RuntimePlatform.Android)
			{
				pluginClass.Call("ActivateReporting", !pauseStatus);
			}
		}

		/// <summary>
		/// Adds a listener that gets called when the native android plugin updates location. 
		/// </summary>
		/// <remarks>
		/// TODO: check if listener is already before adding again added and handle any errors. 
		/// </remarks>
		/// <example> 
		/// This sample shows how to call the <see cref="AddListener"/> and <see cref="RemoveListener"/> methods.
		/// <code>
		/// using UnityEngine;
		/// using UnityEngine.Events;
		/// using System.Collections;
		/// using GeocachingGamePlugin;
		/// 
		/// [RequireComponent(typeof (GeocachingPlugin))]
		/// public class ExampleClass : MonoBehaviour
		/// {
		/// 	GeocachingPlugin geoPlugin;
		/// 
		/// 	void Start()
		/// 	{
		/// 		geoPlugin = GetComponent<GeocachingPlugin>();
		/// 
		/// 		geoPlugin.AddListener(LocationListener);
		/// 	}
		/// 
		///		void Destroy()
		///		{
		///			geoPlugin.RemoveListener(LocationListener);
		///		}
		/// 
		///		void LocationListener(double lat, double lon, int distanceInCentimeters) {
		///			// Do stuff Here...
		///		}
		/// }
		/// </code>
		/// </example>
		/// <param name="listener">Method to add.</param>
		public void AddListener(Action<double, double, int> listener)
		{
			aListeners += listener;
        }

		/// <summary>
		/// Removes a listener that gets called when the native android plugin updates location. 
		/// </summary>
		/// <remarks>
		/// TODO: check if listener is already added before removing and handle any errors. 
		/// </remarks>
		/// <example> 
		/// This sample shows how to call the <see cref="AddListener"/> and <see cref="RemoveListener"/> methods.
		/// <code>
		/// using UnityEngine;
		/// using UnityEngine.Events;
		/// using System.Collections;
		/// using GeocachingGamePlugin;
		/// 
		/// [RequireComponent(typeof (GeocachingPlugin))]
		/// public class ExampleClass : MonoBehaviour
		/// {
		/// 	GeocachingPlugin geoPlugin;
		/// 
		/// 	void Start()
		/// 	{
		/// 		geoPlugin = GetComponent<GeocachingPlugin>();
		/// 
		/// 		geoPlugin.AddListener(LocationListener);
		/// 	}
		/// 
		///		void Destroy()
		///		{
		///			geoPlugin.RemoveListener(LocationListener);
		///		}
		/// 
		///		void LocationListener(double lat, double lon, int distanceInCentimeters) {
		///			// Do stuff Here...
		///		}
		/// }
		/// </code>
		/// </example>
		/// <param name="listener">Method to remove.</param>
		public void RemoveListener(Action<double, double, int> listener)
		{
			aListeners -= listener;
		}

		/// <summary>
		/// Sets distance travelled to 0. 
		/// </summary>
		public void ResetDistanceTravelled()
		{
			totalDistanceTravelled = 0;
		}

		/// <summary>
		/// Converts a World position in latitude/longitude format to the position in OpenStreetMap tiles.
		/// </summary>
		/// <param name="lat">Latitude in degrees.</param>
		/// <param name="lon">Longitude in degrees.</param>
		/// <param name="zoom">Zoom level according to OpenStreetMaps system.</param>
		/// <returns>Tile position according to OpenStreetMaps system.</returns>
		public static Vector2 WorldToTilePos(double lat, double lon, int zoom)
		{
			Vector2 p = new Vector2();
			p.x = (float)((lon + 180.0) / 360.0 * (1 << zoom));
			p.y = (float)((1.0 - Math.Log(Math.Tan(lat * Math.PI / 180.0) +
				1.0 / Math.Cos(lat * Math.PI / 180.0)) / Math.PI) / 2.0 * (1 << zoom));

			return p;
		}

		/// <summary>
		/// Updates the publicly available location values provided by this object.
		/// </summary>
		/// <param name="lat">Latitude in degrees.</param>
		/// <param name="lon">Longitude in degrees.</param>
		/// <param name="distanceInCentimeters">Distance in centimeters to add to the distance counter.</param>
		protected void UpdateLocation(double lat, double lon, int distanceInCentimeters)
		{
			if (distanceInCentimeters > 1) // TODO: Make this take less steps
			{
				totalDistanceTravelled += distanceInCentimeters / 100f;
			}
			aListeners.Invoke(lat, lon, distanceInCentimeters);
        }

		/// <summary>
		/// Internal class that receives callbacks from the native android plugin. 
		/// </summary>
		class LocationPluginListener : AndroidJavaProxy
		{
			/// <summary>
			/// Reference to the active GeocachingPlugin script. 
			/// </summary>
			GeocachingPlugin geoPlugin;

			/// <summary>
			/// Returns a reference to the active GeocachingPlugin script. 
			/// </summary>
			/// <param name="playerLocation"></param>
			public LocationPluginListener(GeocachingPlugin geocachingPlugin) :
				base("com.deovolentegames.geocachingunityplugin.GeocachingGamePlugin$LocationPluginListener")
			{
				geoPlugin = geocachingPlugin;
			}

			/// <summary>
			/// Callback from the native android plugin that contains values to update this GeocachingPlugin object.
			/// </summary>
			/// <param name="lat">Latitude in degrees.</param>
			/// <param name="lon">Longitude in degrees.</param>
			/// <param name="distanceInCentimeters">Distance in centimeters.</param>
			public void OnReceiveLocation(double lat, double lon, int distanceInCentimeters)
			{
				Debug.LogFormat("[LocationPluginListener] {0}, {1}", lat, lon);

				if (geoPlugin == null)
				{
					Debug.LogError("[LocationPluginListener] geoPlugin is null.");
					return;
				}

				geoPlugin.UpdateLocation(lat, lon, distanceInCentimeters);
			}
		}
	}
}
