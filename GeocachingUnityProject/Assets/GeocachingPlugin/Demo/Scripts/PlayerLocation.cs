﻿using UnityEngine;
using UnityEngine.UI;
using MapzenGo.Models;
using GeocachingAndroidPlugin;

/// <summary>
/// Player script that combines MapzenGo map renderer and Geocaching Plugin. 
/// This is just a demo script to show how it works. 
/// </summary>
[RequireComponent(typeof(GeocachingPlugin))]
public class PlayerLocation : MonoBehaviour
{
	public DynamicTileManager tileManager;
	public GeocachingPlugin geoPlugin;

	public float latitude = 51.4779f;
	public float longitude = 0.0014f;
	private float totalDistanceTravelled; 
	
	void Start ()
	{
		totalDistanceTravelled = 0;

		// Check the MapzenGo TileManager
		if (tileManager == null)
		{
			Debug.LogWarning("CachedDynamicTileManager is not linked in PlayerLocation script");
			tileManager = FindObjectOfType<DynamicTileManager>();
			if (tileManager == null)
			{
				Debug.LogError("Cannot find CachedDynamicTileManager");
			}
		}

		// Check if we are running on an android device, not really necessary
		if (Application.platform != RuntimePlatform.Android)
		{
			Debug.LogWarning("Not running on an Android device!");
		}

		// Check GeocachingPlugin and add the listener
		if (geoPlugin == null)
		{
			geoPlugin = GetComponent<GeocachingPlugin>();
		}
		geoPlugin.AddListener(UpdateLocation);
		Debug.Log("[Callback] Added listener");

		tileManager.Latitude = latitude;
		tileManager.Longitude = longitude;
	}

	void Destroy()
	{
		// Remember to remove the listener when you are done with it!
		geoPlugin.RemoveListener(UpdateLocation);
	}

	/// <summary>
	/// Update location listener method. Has to have this exact declaration or the java plugin won't work. 
	/// </summary>
	/// <param name="lat">Latitude as a double.</param>
	/// <param name="lon">Longitude as a double.</param>
	/// <param name="distanceInCentimeters">Distance travelled in centimeters recorded since the last listener update.</param>
	public void UpdateLocation(double lat, double lon, int distanceInCentimeters)
	{
		tileManager.Latitude = latitude = (float)lat;
		tileManager.Longitude = longitude = (float)lon;

		totalDistanceTravelled = geoPlugin.totalDistanceTravelled;
		
		DebugText.Log(string.Format("lat: {0}, lon: {1}, time: {2}, pos: {3}, dist: {4}",
			lat.ToString("F7"),
			lon.ToString("F7"),
			Time.realtimeSinceStartup,
			transform.position.ToString("F2"),
			totalDistanceTravelled));
		//Debug.LogFormat("[Callback] OnReceiveLocation completed \"successfully\", lat: {0}, lon: {1}, dist: {2}", lat, lon, totalDistanceTravelled);
	}
}
