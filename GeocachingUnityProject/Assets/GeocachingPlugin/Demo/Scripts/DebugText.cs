﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugText : MonoBehaviour {

	public Text textElement; 
	public static DebugText debugText;

	void Awake()
	{
		if (debugText == null)
		{
			debugText = this;
		}
		else
		{
			Debug.LogWarning("There is more than one DebugText instance in the scene!");
		}

		if (textElement == null)
		{
			Debug.LogError("There is no UI Text element connected to DebugText");
		}
    }
	
	public static void Log(string message)
	{
		if (debugText == null)
		{
			Debug.LogError("There is no DebugText instance in the scene!");
		}
		else if (debugText.textElement == null)
		{
			Debug.LogError("There is no UI Text element connected to DebugText");
		}
		else
		{
			debugText.textElement.text = message;
		}
	}
}
