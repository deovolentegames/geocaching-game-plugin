﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class OrbitTouchControl : MonoBehaviour, IDragHandler
{
	public float dragSpeed = 0.2f;

	private Transform cameraTransform; 

	void Start()
	{
		cameraTransform = Camera.main.transform;
    }

	public void OnDrag(PointerEventData eventData)
	{
		cameraTransform.RotateAround(Vector3.zero, Vector3.up, eventData.delta.x * dragSpeed);
    }

	public void FaceAngle(float angle)
	{
		float diff = angle - cameraTransform.eulerAngles.y;
        cameraTransform.RotateAround(Vector3.zero, Vector3.up, diff);
	}
}