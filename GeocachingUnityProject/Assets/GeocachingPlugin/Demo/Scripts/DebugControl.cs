﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PlayerLocation))]
public class DebugControl : MonoBehaviour
{
#if UNITY_EDITOR
	public float velocity = 0.01f; 

	private PlayerLocation pLoc;
	private Vector2d latLonPos;

	void Start()
	{
		pLoc = GetComponent<PlayerLocation>();
		latLonPos = new Vector2d(pLoc.latitude, pLoc.longitude);
    }

	void Update()
	{
		latLonPos += new Vector2d(Input.GetAxis("Vertical"), Input.GetAxis("Horizontal")) * velocity * Time.deltaTime;
		pLoc.UpdateLocation(latLonPos.x, latLonPos.y, 0);
	}
#endif
}
