var hierarchy =
[
    [ "ConnectionCallbacks", null, [
      [ "com.deovolentegames.geocachingunityplugin.GeocachingGameService", "classcom_1_1deovolentegames_1_1geocachingunityplugin_1_1_geocaching_game_service.html", null ]
    ] ],
    [ "com.deovolentegames.geocachingunityplugin.GeocachingGamePlugin", "classcom_1_1deovolentegames_1_1geocachingunityplugin_1_1_geocaching_game_plugin.html", null ],
    [ "IDragHandler", null, [
      [ "OrbitTouchControl", "class_orbit_touch_control.html", null ]
    ] ],
    [ "com.deovolentegames.geocachingunityplugin.GeocachingGamePlugin.LocationPluginListener", "interfacecom_1_1deovolentegames_1_1geocachingunityplugin_1_1_geocaching_game_plugin_1_1_location_plugin_listener.html", null ],
    [ "MonoBehaviour", null, [
      [ "DebugControl", "class_debug_control.html", null ],
      [ "DebugText", "class_debug_text.html", null ],
      [ "GeocachingAndroidPlugin.GeocachingPlugin", "class_geocaching_android_plugin_1_1_geocaching_plugin.html", null ],
      [ "OrbitTouchControl", "class_orbit_touch_control.html", null ],
      [ "PlayerLocation", "class_player_location.html", null ]
    ] ],
    [ "OnConnectionFailedListener", null, [
      [ "com.deovolentegames.geocachingunityplugin.GeocachingGameService", "classcom_1_1deovolentegames_1_1geocachingunityplugin_1_1_geocaching_game_service.html", null ]
    ] ],
    [ "Binder", null, [
      [ "com.deovolentegames.geocachingunityplugin.GeocachingGameService.LocalBinder", "classcom_1_1deovolentegames_1_1geocachingunityplugin_1_1_geocaching_game_service_1_1_local_binder.html", null ]
    ] ],
    [ "LocationListener", null, [
      [ "com.deovolentegames.geocachingunityplugin.GeocachingGameService", "classcom_1_1deovolentegames_1_1geocachingunityplugin_1_1_geocaching_game_service.html", null ]
    ] ],
    [ "Service", null, [
      [ "com.deovolentegames.geocachingunityplugin.GeocachingGameService", "classcom_1_1deovolentegames_1_1geocachingunityplugin_1_1_geocaching_game_service.html", null ]
    ] ]
];