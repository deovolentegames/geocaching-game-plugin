package com.deovolentegames.geocachingunityplugin;

import android.Manifest;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;
import java.util.Locale;

/**
 * <h1>Geocaching Game Service</h1>
 * The GeocachingGameService is a background service that receives location data from the Google
 * Location Service and sends the latitude, longitude and distance travelled to listeners.
 *
 * <p>It is designed to be used primarily with Unity to solve the problem in geocaching games where
 * distance travelled while the phone's screen is off or the app is otherwise not in the foreground
 * doesn't count.
 *
 * <p>{@link GeocachingGamePlugin} is the actual entry point for the Unity application so it can
 * send information to this service.
 *
 * <p>Finally, here's the references that I used to create this code (mostly here for my own sake):
 * <ul>
 * <li><a href="http://stackoverflow.com/a/38369904">
 *     Start Android Service from Unity3D.</a></li>
 * <li><a href="http://stackoverflow.com/a/7775771">
 *     Set a Listener in a Service-based Class</a></li>
 * <li><a href="http://stackoverflow.com/a/8019196">
 *     Service call backs to activity using BindService().</a></li>
 * <li><a href="https://developer.android.com/guide/components/bound-services.html">
 *     Bound Services documentation.</a></li>
 * <li><a href="http://stackoverflow.com/a/13787699">
 *     Why I use bind (not start) for GeocachingGameService.</a></li>
 * <li><a href="http://stackoverflow.com/a/41018028">
 *     Callback Listener in Unity using AndroidJavaProxy.</a></li>
 * <li><a href="http://stackoverflow.com/a/7145022">
 *     How is an Intent Service Declared in the Android Manifest?</a></li>
 * </ul>
 *
 * @author  Rory
 * @version 1.0
 * @since   2017-01-21
 * @see GeocachingGamePlugin
 * @see <a href="http://deovolente.games/">deovolente.games</a>
 */
public class GeocachingGameService extends Service implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    //region VARIABLES
    public static final String TAG = GeocachingGameService.class.getSimpleName();

    // Service-client stuff
    private final IBinder binder = new LocalBinder();
    private final ArrayList<GeocachingGamePlugin.LocationPluginListener> listeners
            = new ArrayList<>();
    private boolean notifyListeners = true;

    // Location stuff
    private final static double AVERAGE_RADIUS_OF_EARTH = 637100000; // in centimeters
    private GoogleApiClient googleApiClient;
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private LocationRequest locationRequest;
    public static int locationInterval = 5000; // 5 seconds in milliseconds
    public static int locationFastestInterval = 1000; // 1 second in milliseconds
    public static float locationAccuracy = 20; // in meters
    public static float minSpeed = 0.1f; // in m/s
    public static float maxSpeed = 5f; // in m/s

    // Distance stuff
    private int cachedDistance; // in cm
    private Location lastLocation;
    //endregion

    //region METHOD OVERRIDES
    @Override
    public void onCreate() {
        super.onCreate();

        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        googleApiClient.connect();

        // TODO: Change accuracy when in background
        locationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(locationInterval)
                .setFastestInterval(locationFastestInterval);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (googleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
            googleApiClient.disconnect();
        }

        Log.i(TAG, "Service destroyed.");
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling ActivityCompat#requestPermissions here
            // to request the missing permissions, and then overriding
            // public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                        int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            Log.i(TAG, "Missing location permissions.");
            return;
        }
        lastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        LocationServices.FusedLocationApi.requestLocationUpdates(
                googleApiClient, locationRequest, this);
        Log.i(TAG, "Location services connected.");
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG, "Location services suspended. Please reconnect.");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        /*
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(this,
                CONNECTION_FAILURE_RESOLUTION_REQUEST); // Get Unity Activity
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        } else {
            Log.e(TAG, "Location services connection failed with code " +
                    connectionResult.getErrorCode());
        }
        */
        Log.e(TAG, "Location services connection failed with code " +
                connectionResult.getErrorCode());
    }

    @Override
    public void onLocationChanged(Location location) {
        handleNewLocation(location);
    }
    //endregion

    //region METHODS
    /**
     * This method allows other classes to register listeners to this service that get called when
     * the location is updated.
     * @param listener This is the listener to add.
     */
    public void registerListener(GeocachingGamePlugin.LocationPluginListener listener) {
        listeners.add(listener);
        Log.i(TAG, "Listener added.");
    }

    /**
     * This method allows other classes to unregister their registered listeners.
     * @param listener This is the listener to remove.
     */
    public void deregisterListener(GeocachingGamePlugin.LocationPluginListener listener) {
        listeners.remove(listener);
        Log.i(TAG, "Listener removed.");
    }

    /**
     * This method activates or deactivates reporting data to listeners.
     * @param isActive Activates reporting to listeners when true, deactivates when false.
     */
    public void activateReporting(boolean isActive)
    {
        notifyListeners = isActive;

        if (isActive)
        {
            handleNewLocation(lastLocation);
        }
    }

    /**
     * This is the method that handles new location data and calls registered listeners
     * @param location The location data to update listeners with.
     */
    private void handleNewLocation(Location location) {
        float tempSpeed = -1f; // Speed in m/s, -1 means uninitialised

        // TODO: Create a better way to update listeners than using this
        if (lastLocation != location) {
            if (lastLocation != null) {
                int tempDistance = calculateDistance(lastLocation, location);

                if (location.hasSpeed())
                {
                    tempSpeed = location.getSpeed();
                }
                else
                {
                    tempSpeed = tempDistance * 10000000f / (location.getElapsedRealtimeNanos() -
                            lastLocation.getElapsedRealtimeNanos());
                }

                // TODO: Fix  position sensitivity calculations so they can handle spikes.
                if (location.hasAccuracy() && location.getAccuracy() < locationAccuracy &&
                        tempSpeed > minSpeed && tempSpeed < maxSpeed) {
                    cachedDistance += tempDistance;
                }
            }
            lastLocation = location;
        }

        String debugString = String.format(Locale.ENGLISH, "Handling location: %1$s, dist: %2$d, " +
                "avgSpeed: %3$.3f, notify: %4$b",
                location.toString(), cachedDistance, tempSpeed, notifyListeners);
        Log.d(TAG, debugString);

        if (notifyListeners) {
            for (int i = listeners.size() - 1; i >= 0; i--) {
                listeners.get(i).OnReceiveLocation(location.getLatitude(),
                        location.getLongitude(), cachedDistance);
            }
            cachedDistance = 0;
        }
    }

    private int calculateDistance(Location startLoc, Location endLoc) {
        return calculateDistance(
                startLoc.getLatitude(), startLoc.getLongitude(),
                endLoc.getLatitude(), endLoc.getLongitude());
    }
    private int calculateDistance(double startLat, double startLon, double endLat, double endLon) {

        double latDistance = Math.toRadians(startLat - endLat);
        double lngDistance = Math.toRadians(startLon - endLon);

        double a = (Math.sin(latDistance / 2) * Math.sin(latDistance / 2)) +
                (Math.cos(Math.toRadians(startLat))) *
                        (Math.cos(Math.toRadians(endLat))) *
                        (Math.sin(lngDistance / 2)) *
                        (Math.sin(lngDistance / 2));

        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        return (int) (Math.round(AVERAGE_RADIUS_OF_EARTH * c));

    }
    //endregion

    //region CLASSES
    /**
     * Custom binder so that methods can be called on this Service.
     */
    public class LocalBinder extends Binder {
        GeocachingGameService getService() {
            return GeocachingGameService.this;
        }
    }
    //endregion
}