package com.deovolentegames.geocachingunityplugin;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;

/**
 * <h1>Geocaching Game Plugin</h1>
 * Executing methods from an instance of this object is the main way  for external applications
 * (namely Unity) to interface with the {@link GeocachingGameService} that runs in the background.
 *
 * <p>More information about the project can be found in {@link GeocachingGameService}, which
 * contains the bulk of this library's function.
 *
 * @author  Rory
 * @version 1.0
 * @since   2017-01-21
 * @see GeocachingGameService
 * @see <a href="http://deovolente.games/">deovolente.games</a>
 */
public class GeocachingGamePlugin {
    public static final String TAG = GeocachingGamePlugin.class.getSimpleName();

    private LocationPluginListener callback;
    private GeocachingGameService geocachingGameService;
    private boolean isBound = false;

    private ServiceConnection connection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            GeocachingGameService.LocalBinder binder = (GeocachingGameService.LocalBinder) service;
            geocachingGameService = binder.getService();
            geocachingGameService.registerListener(callback);
            isBound = true;
        }

        // Check this later, atm it feels like cargo cult programming
        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            geocachingGameService.deregisterListener(callback);
            isBound = false;
        }
    };

    public void StartLocationCacheService(Activity unityActivity, LocationPluginListener callback) {
        this.callback = callback;

        Intent intent = new Intent(unityActivity, GeocachingGameService.class);
        unityActivity.bindService(intent, connection, Context.BIND_AUTO_CREATE);
    }

    public void SetConfiguration(float locationAccuracy, float minSpeed, float maxSpeed) {
        GeocachingGameService.locationAccuracy = locationAccuracy;
        GeocachingGameService.minSpeed = minSpeed;
        GeocachingGameService.maxSpeed = maxSpeed;

        Log.i(TAG, "Set location service configuration.");
    }

    public void SetConfiguration(float locationAccuracy, float minSpeed, float maxSpeed,
                                 int locationInterval, int locationFastestInterval) {
        SetConfiguration(locationAccuracy, minSpeed, maxSpeed);
        GeocachingGameService.locationInterval = locationInterval;
        GeocachingGameService.locationFastestInterval = locationFastestInterval;
    }

    public void ActivateReporting(boolean isActive) {
        if (isBound)
            geocachingGameService.activateReporting(isActive);
    }

    public interface LocationPluginListener {
        void OnReceiveLocation(double lat, double lon, int distInCM);
    }
}

