# Geocaching Game Plugin for Unity

A simple plugin that is designed to provide geolocation data to a Unity game, even when the screen is off.

---

## Important Caveats

**This isn't a real production plugin**, it's more of a learning tool or proof of concept.
If you want to use something like this it would be smart to research the topic and make something that works for your specific project.

It also **only works with Android devices**, not iOS or Windows.

Finally, be aware that the current version of this plugin (as of v1.0) doesn't contain any way to throttle location updates or stop them after a period of time.
Because of this it will **run forever in the background**, get very hot and eat a lot of battery life. This is something I'd love to change in future, but for now you'll just have to deal with it!

---

## Overview

This was created as a way for me to learn about making Android services and using them with Unity plugins.
It's also a way of me to resolve my issues with Pokémon GO, the most significant (apart from the lack of battle mechanics) is that I feel that the geolocation mechanics are poorly implemented.
However, I feel that I have no basis to make these claims until I have attempted to try and develop this feature myself.

You can download the releases [here](https://gitlab.com/deovolentegames/geocaching-game-plugin/tags).
<sup>I'd love to [link to the latest release](https://help.github.com/articles/linking-to-releases/) but I can't do that with GitLab yet.</sup>

## Documentation

As this is focused on being a learning tool I tried to comment all my code in both the Unity project, demo and the Android native plugin (and I added comments to a little bit of the MapzenGo Code as well); you can find the generated documentation [here](https://deovolentegames.gitlab.io/geocaching-game-plugin).
It's pretty ugly at the moment but it's mostly readable.
If there's anything that doesn't make sense, is missing, is wrong or is just plain broken please let me know, I'll try and fix it as soon as I can.

## Tutorial?

Nope. Not yet at least. For now all I've got is a list of my references [at the bottom of this readme][tuts-and-refs] and [in the documentation][doc-refs] (GitLab source [here](GeocachingUnityPlugin/app/src/main/java/com/deovolentegames/geocachingunityplugin/GeocachingGameService.java#L26)), alongside the other documentation that I'm gradually adding to.
When I'm done with the docs I'll write up a real tutorial blog post [on my website](http://deovolente.games/).

## Systems

The Unity part of the project was created with version 5.5.0 and the Android part was created with Android Studio version 2.3.
The Git repo uses Git LFS and the editor I'm currently using is Visual Studio Community 2015.

I also used [MapzenGo][MapzenGo], an open source Pokémon Go styled 3D map renderer created by Baran Kahyaoglu using the Mapzen service. I altered it a bit though (because I felt like it).
The guy who made it is now working on the official [Mapzen Unity SDK](https://www.mapbox.com/unity/) so definitely take a look at that.
In future I'd like to try integrating the official SDK but it wasn't practical when I started.
If you want more info on the MapzenGo project check out [the repo][MapzenGo] or  [Baran's blog][Baran].

## Todo

1. Provide better ways of reducing battery usage. *This is very important.*
2. Smooth distance calculations.
3. Smooth player movement and camera rotations. *Not important, just looks nice.*

## Tutorials and References

These are the tutorials and references that I used to learn how to create this project.
If you're interested in creating something like this

1. [Plugins for Android - Unity Manual](https://docs.unity3d.com/Manual/PluginsForAndroid.html).
2. [Communication Between an Android App and Unity](http://jeanmeyblum.weebly.com/scripts--tutorials/communication-between-an-android-app-and-unity).
3. [Writing Plugins - Official Unity Tutorial with Mike Geig](https://unity3d.com/learn/tutorials/topics/scripting/writing-plugins) - video length 1:05.
4. [Creating a native Android plugin for Unity3d](http://www.what-could-possibly-go-wrong.com/creating-a-native-android-plugin-for-unity3d/).
5. [`TYPE_STEP_COUNTER` - Android API](https://developer.android.com/reference/android/hardware/Sensor.html#TYPE_STEP_COUNTER).
6. [Create An Android Plugin For Unity Using Android Studio](http://www.thegamecontriver.com/2015/04/android-plugin-unity-android-studio.html).
7. [Start android service from Unity3D code - Stack Overflow](http://stackoverflow.com/a/38369904).
8. [Set a Listener in a Service-based Class - Stack Overflow](http://stackoverflow.com/a/7775771).
9. [Bound Services](https://developer.android.com/guide/components/bound-services.html).
10. [MapzenGo][MapzenGo], [Baran Kahyaoglu's blog](Baran).

[doc-refs]: https://deovolentegames.gitlab.io/geocaching-game-plugin/classcom_1_1deovolentegames_1_1geocachingunityplugin_1_1_geocaching_game_service.html#details
[tuts-and-refs]: https://gitlab.com/deovolentegames/geocaching-game-plugin#tutorials-and-references
[MapzenGo]: https://github.com/brnkhy/MapzenGo
[Baran]: http://barankahyaoglu.com/dev/
